itcrispr
========
Interface to assemble and track progress of CRISPR sequence experiments in 
BioNumerics (6.5 - 6.6).
